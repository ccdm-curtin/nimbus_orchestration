# Openstack orchestration templates

This repository contains templates for setting up virtual servers and clusters in openstack.
Scripts are designed to be compatible with existing resources in the [Nimbus cloud service](https://support.pawsey.org.au/documentation/display/US/Nimbus%2C+the+research+cloud+at+Pawsey) operated by the [Pawsey supercomputing centre](https://www.pawsey.org.au/), but may be compatible with other openstack clouds.


For more documentation on how to use these templates, please look at the [wiki page](https://bitbucket.org/ccdm-curtin/nimbus_orchestration/wiki/Home).


## Orchestration types implemented

This is a quick summary of what's on offer.
These template should serve the vast majority of simple uses for nimbus.

All servers are Ubuntu Xenial (16.04) based and automatically configured to update software and install some useful command line tools like htop, tmux, vim etc.

Additionally for interactive single servers, users can optionally install some commonly used software and package managers:

- The official [R](https://cran.r-project.org/) ubuntu [apt repository](https://cran.r-project.org/bin/linux/ubuntu/) and [Rstudio server](https://www.rstudio.com/products/RStudio/), which will run automatically.
- [Docker CE Engine](https://www.docker.com/)
- The [miniconda](https://conda.io/docs/) package manager in the 'ubuntu' users home directory, with external channels enabled by default (e.g. [bioconda](https://bioconda.github.io/)).
- The [Nextflow](https://www.nextflow.io/) pipeline language/platform.


A simple shared file system (a bit like the R-drive) template is available, using the [network file system](https://en.wikipedia.org/wiki/Network_File_System) protocol.
Multiple other servers and users in the same network can automatically attach this as a volume using options in the templates, or by adding a simple fstab line (see wiki).


## On the wish list

I hope to implement some more complex templates to automatically setup small clusters or servers optimised for specific commonish tasks.

Planned:

- Clusters based on [Nextflow](https://www.nextflow.io/) using [Apache Ignite](https://ignite.apache.org/) as a backend.
- A template to setup a simple BLAST environment, e.g. for whole genome blast's etc.
- A template to automate installation of Interproscan with its dependencies and data sources.

Other options that people might like are things like [Docker swarms](https://docs.docker.com/engine/swarm/) or small [SLURM based clusters](http://gc3-uzh-ch.github.io/elasticluster/).
I have no plans to put these together in the short term, but if it's something you're interested in, we can talk.

If you have other needs please contact me (Darcy) or create an issue in the Git repository.


## What if I don't want to use the command line interface?

I know lots of people are intimidated by the command line, so we have some templates that you can start from the nimbus web browser.
These templates will only really cover the simple cases for single instances.
For more complex templates, you'll need to use the command line interface, which is extremely copy-pasteable anyway :).

You can find these foolproof templates in the `foolproof` folder.
Please see the [wiki for instructions on how to use them](https://bitbucket.org/ccdm-curtin/nimbus_orchestration/wiki/running_foolproof_templates).


## Quick start

This is for command line users.
For those wanting more detail or to use the web, please see the wiki.

1) Install the openstack and heat clients

```
# In ubuntu...
sudo apt install python-openstackclient python-heatclient
```

Make sure you install the versions for **python2**.
Python3 is not fully supported yet :(

2) Download an RC file from <https://nimbus.pawsey.org.au/horizon/project/api_access/openrc/>.
   Use a web browser; you'll need to log in.

3) Source the RC file.

```
# e.g.
source ~/Downloads/fungal-pathogens-openrc.sh
```

The file will ask you for your password, which is the same as your pawsey password/the one you use to log in to the website.
Sourcing this file tells the openstack client how to connect to nimbus, and how to show them who you are.
You will need to do this for any terminal session window that you use, but it will be inherited by terminal session managers etc if you start them from a terminal that has sourced the file (e.g. tmux).

4) Check out what's running in your openstack project.

```
# Show all running instances
openstack server list

# Show all current volumes
openstack volume list

# Show all available ssh keypairs
openstack keypair list
```

Make a note of the name of one of the keypairs available to you.
If you don't have a keypair, follow the instructions here <https://support.pawsey.org.au/documentation/display/US/Nimbus+-+Launching+an+Instance> under 'Configure Security Groups and SSH Keys'.
Just the ssh key bit, you don't need to add security groups.

5) Clone this repository

```
git clone git@bitbucket.org:ccdm-curtin/nimbus_orchestration.git && cd nimbus_orchestration
```

6) Start your first stack

A stack is a group of different components which are created and (usually) deleted together.
Stacks can contain servers, volumes, floating ips, networks...
Pretty much anything that openstack does can be part of a stack.
The templates in this project are templates for creating stacks.

Let's create a simple stack with a single instance.
Replace the bit that says `<my_key_name>` with the key pair name from the last step, and `<mystack>` with the name that you want to call your stack.

```
openstack stack create -t templates/server.yaml -e templates/server_env.yaml --parameter "key_name=<your_key_name>" <mystack>
```
You should be able to now see your stack in the list of stacks...

```
openstack stack list
openstack stack show <mystack>
```

You can now look at how the individual parts of the stack are being created.

```
openstack stack event list <mystack>
```

7) Connect to your server

You can find the **Public** (syn floating) ip address either from the output of `openstack stack show <mystack>` or `openstack server list`.
We can use this ip address to ssh into the instance.

e.g. A public IP might be 146.118.65.108

```
ssh -i ~/path/to/my_key.pem ubuntu@146.118.65.108
```

Hopefully you're now connected to the terminal in the remote machine!


## HTTP connections

Rather than expose a an http/https port to the web which would require some additional security precautions, I'm suggesting that people use local ssh port forwarding to access http based services like rstudio server.

Essentially this connects a port on the remote machine to a port on your local machine, and transmits the information through the ssh connection (and as such is encrypted and requires an ssh key).
Once you've set up the forwarding, you can connect to it via localhost as if it was running on yourown computer.

Rstudio server runs on port 8787, so we'll connect that to port 8080 on our local computer.

```
ssh -i ~/path/to/my_key.pem -L 8080:localhost:8787 -N ubuntu@146.118.65.108
```

Taking this apart a bit. 
`-L` specifies that ssh should attempt a local port forward (there are other types).
`8080:localhost:8787` specifies that we should attach port 8787 from the **remote** localhost domain (ie not connected to any external networks), to port 8080 on our local machine.
The localhost bit in that statement refers to the network on the remote machine, with local forwarding the port will always come to your localhost domain.

Open up your browser and enter the url: <http://localhost:8080>

With some luck you should see a login window appear.
The user is `ubuntu`, and the password can be found in the output of `openstack stack show <mystack>`.
Then you should have access to Rstudio server running on a remote machine.

One thing to be wary of is that the port forwarding ends if you stop the ssh process from running.
So you might like to run the command in a session manager like `tmux` or `screen`, so that the proess can run in the background, and you don't have to worry about accidentally killing it.

Another thing that might occur is losing connections (e.g. if you leave the connection on and your computer goes into sleep).
Sometimes this you won't be able to reconnect the remote port to the same port on your local machine, so you just have to change it e.g. to 8081 or 8787 etc.
Don't worry, if you lose the connection you haven't lost your work, if you reconnect you should be able to pick up pretty much where you left.


## How does this template thing work anyway?

There are lots of ways to automate deployment of openstack servers etc.
I've chosen to use [HEAT](https://docs.openstack.org/heat/latest/) mostly because there are _some_ tools for handling stacks using the openstack web interface ([horizon](https://docs.openstack.org/horizon/latest/)).
Hopefully this lowers the barrier of entry for new users.

[Ansible](https://www.ansible.com/) is probably the best known orchestration platform, but again we want to make it easy for people to get started, which means a web interface.

So heat templates (HEAT orchestration templates; HOT) are based on YAML config files, and borrows heavily from [Amazons cloud formation](https://aws.amazon.com/cloudformation/) template system.
HOT also supports software/virtual machine configuration using [cloud-init](http://cloud-init.org/) and its yaml based [cloud-config](http://cloudinit.readthedocs.io/en/latest) syntax.

The template files themselves are reasonably heavily commented with links to different bits of information.
If you have questions or want to contribute and don't know where to start, please ask me (Darcy) or someone else who is already involved, or use the issue tracker in the git repo. 


Toodles!
